\select@language {ngerman}
\contentsline {section}{Abbildungsverzeichnis}{iii}{section*.3}
\contentsline {section}{Tabellenverzeichnis}{iii}{section*.4}
\contentsline {section}{Abk\IeC {\"u}rzungsverzeichnis}{iv}{section*.5}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Problemstellung}{1}{section.2}
\contentsline {section}{\numberline {3}Entwicklung des mathematisches Modells}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Modellannahmen}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Notation und Optimierungsmodell}{3}{subsection.3.2}
\contentsline {section}{\numberline {4}Entwicklung der Anwendung}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Beschreibung der Web-Applikation}{4}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Start der Oberfl\IeC {\"a}che}{4}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Aufbau der Web-Applikation}{7}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Grundlegende Funktionen}{11}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}GAMS-Implementierung und Systemarchitektur}{16}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Verkn\IeC {\"u}pfung von GAMS und der Web-Applikation}{16}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Datenbankmodell}{19}{subsubsection.4.2.2}
\contentsline {section}{\numberline {5}Test-Suit}{22}{section.5}
\contentsline {section}{\numberline {6}Exemplarischer Workflow}{24}{section.6}
\contentsline {section}{\numberline {7}Limitationen und Handlungsempfehlungen}{27}{section.7}
\contentsline {section}{\numberline {8}Fazit und Ausblick}{29}{section.8}
