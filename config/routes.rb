Rails.application.routes.draw do
  resources :sites
  resources :preferencesites
  resources :translinks
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
root 'static_pages#login'
get  '/home',    to: 'static_pages#home'
get  '/help',    to: 'static_pages#help'
get  '/about',   to: 'static_pages#about'
get  '/contact', to: 'static_pages#contact'
get '/optimize',  to: 'static_pages#optimize'
get '/institute', to: 'static_pages#institute'  
get  '/signup',  to: 'users#new'
post '/signup',  to: 'static_pages#home'
get    '/login',   to: 'sessions#new'
post   '/login',   to: 'sessions#create'
delete '/logout',  to: 'sessions#destroy'
resources :users

post 'translinks/delete_zuordnung', :to => 'translinks#delete_zuordnung'
post 'translinks/read_zuordnung', :to => 'translinks#read_zuordnung'
post 'translinks/optimize', :to => 'translinks#optimize'

end
