class CreateSites < ActiveRecord::Migration[5.1]
  def change
    create_table :sites do |t|
      t.string :name
      t.float :prof
      t.float :hiwi

      t.timestamps
    end
  end
end
