class CreateTranslinks < ActiveRecord::Migration[5.1]
  def change
    create_table :translinks do |t|
      t.integer :preferencesite_id
      t.integer :user_id
      t.binary :zuordnung

      t.timestamps
    end
    add_foreign_key(:translinks, :preferencesite, column: 'preferencesite_id')
    add_foreign_key(:translinks, :user, column: 'user_id')
  end
end
