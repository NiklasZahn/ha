class AddUserIdToPreferencesites < ActiveRecord::Migration[5.1]
  def change
    add_column :preferencesites, :user_id, :integer
  end
end
