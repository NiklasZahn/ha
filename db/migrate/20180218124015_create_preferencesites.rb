class CreatePreferencesites < ActiveRecord::Migration[5.1]
  def change
    create_table :preferencesites do |t|
      t.string :name
      t.float :preference

      t.timestamps
    end
  end
end
