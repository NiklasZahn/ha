# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

S01 = Site.create!(name: "Arbeitsökonomik", prof: "2", hiwi: "7", capincr: "0.1")
S02 = Site.create!(name: "Banken und Finanzierung", prof: "1", hiwi: "4", capincr: "0")
S03 = Site.create!(name: "Betriebswirtschaftliche Steuerlehre", prof: "1", hiwi: "9", capincr: "0")
S04 = Site.create!(name: "Controlling", prof: "1", hiwi: "7", capincr: "0")
S05 = Site.create!(name: "Entwicklungs- und Agrarökonomik", prof: "2", hiwi: "16", capincr: "0")
S06 = Site.create!(name: "Finanzmarkttheorie", prof: "1", hiwi: "5", capincr: "0")
S07 = Site.create!(name: "Geld- und internationale Finanzwirtschaft", prof: "1", hiwi: "3", capincr: "0")
S08 = Site.create!(name: "Makroökonomik", prof: "1", hiwi: "7", capincr: "0")
S09 = Site.create!(name: "Marketing und Management", prof: "1", hiwi: "9", capincr: "0")
S10 = Site.create!(name: "Mikroökonomik", prof: "1", hiwi: "5", capincr: "0")
S11 = Site.create!(name: "Öffentliche Finanzen", prof: "1", hiwi: "5", capincr: "0")
S12 = Site.create!(name: "Personal und Arbeit", prof: "1", hiwi: "3", capincr: "0")
S13 = Site.create!(name: "Produktionswirtschaft", prof: "1", hiwi: "9", capincr: "0")
S14 = Site.create!(name: "Rechnungslegung und Wirtschaftsprüfung", prof: "1", hiwi: "6", capincr: "0")
S15 = Site.create!(name: "Statistik", prof: "2", hiwi: "8", capincr: "0")
S16 = Site.create!(name: "Umweltökonomik und Welthandel", prof: "2", hiwi: "20", capincr: "0")
S17 = Site.create!(name: "Unternehmensführung und Organisation", prof: "1", hiwi: "7", capincr: "0")
S18 = Site.create!(name: "Versicherungsbetriebslehre", prof: "1", hiwi: "15", capincr: "0")
S19 = Site.create!(name: "Wirtschaftsinformatik", prof: "1", hiwi: "20", capincr: "0")
S20 = Site.create!(name: "Wirtschaftspolitik", prof: "2", hiwi: "13", capincr: "0")

 U1 = User.create!(name:  "Example User",
              email: "example@railstutorial.org",
              matrikelnummer: "1111111",
              password:              "foobar",
              password_confirmation: "foobar",
              admin: false,
              activated: true,
              activated_at: Time.zone.now)

 U2 = User.create!(name:  "Stefan Helber",
             email: "stefan.helber@prod.uni-hannover.de",
             matrikelnummer: "9999999",
             password:              "geheim",
             password_confirmation: "geheim",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

             P01 = Preferencesite.create!(name: "Arbeitsökonomik", user_id: U1.id,
                                preference:"10")
             P02 = Preferencesite.create!(name: "Banken und Finanzierung", user_id: U1.id,
                                preference: "10")
             P03 = Preferencesite.create!(name: "Betriebswirtschaftliche Steuerlehre", user_id: U1.id,
                                preference: "10")
             P04 = Preferencesite.create!(name: "Controlling", user_id: U1.id,
                                preference: "10")
             P05 = Preferencesite.create!(name: "Entwicklungs- und Agrarökonomik", user_id: U1.id,
                                preference: "10")
             P06 = Preferencesite.create!(name: "Finanzmarkttheorie", user_id: U1.id,
                                preference: "10")
             P07 = Preferencesite.create!(name: "Geld- und internationale Finanzwirtschaft", user_id: U1.id,
                                preference: "10")
             P08 = Preferencesite.create!(name: "Makroökonomik", user_id: U1.id,
                                preference: "10")
             P09 = Preferencesite.create!(name: "Marketing und Management", user_id: U1.id,
                                preference: "1")
             P10 = Preferencesite.create!(name: "Mikroökonomik", user_id: U1.id,
                                preference: "10")
             P11 = Preferencesite.create!(name: "Öffentliche Finanzen", user_id: U1.id,
                                preference: "10")
             P12 = Preferencesite.create!(name: "Personal und Arbeit", user_id: U1.id,
                                preference: "10")
             P13 = Preferencesite.create!(name: "Produktionswirtschaft", user_id: U1.id,
                                preference: "10")
             P14 = Preferencesite.create!(name: "Rechnungslegung und Wirtschaftsprüfung", user_id: U1.id,
                                preference: "10")
             P15 = Preferencesite.create!(name: "Statistik", user_id: U1.id,
                                preference: "10")
             P16 = Preferencesite.create!(name: "Umweltökonomik und Welthandel", user_id: U1.id,
                                preference: "10")
             P17 = Preferencesite.create!(name: "Unternehmensführung und Organisation", user_id: U1.id,
                                preference: "10")
             P18 = Preferencesite.create!(name: "Versicherungsbetriebslehre", user_id: U1.id,
                                preference: "10")
             P19 = Preferencesite.create!(name: "Wirtschaftsinformatik", user_id: U1.id,
                                preference: "10")
             P20 = Preferencesite.create!(name: "Wirtschaftspolitik", user_id: U1.id,
                                preference: "10")

P21=Preferencesite.create!(name: "Arbeitsökonomik", user_id: U2.id, preference: "1")
P22=Preferencesite.create!(name: "Banken und Finanzierung", user_id: U2.id, preference: "10")
P23=Preferencesite.create!(name: "Betriebswirtschaftliche Steuerlehre", user_id: U2.id, preference: "10")
P24=Preferencesite.create!(name: "Controlling", user_id: U2.id, preference: "10")
P25=Preferencesite.create!(name: "Entwicklungs- und Agrarökonomik", user_id: U2.id, preference: "10")
P26=Preferencesite.create!(name: "Finanzmarkttheorie", user_id: U2.id, preference: "10")
P27=Preferencesite.create!(name: "Geld- und internationale Finanzwirtschaft", user_id: U2.id, preference: "10")
P28=Preferencesite.create!(name: "Makroökonomik", user_id: U2.id, preference: "10")
P29=Preferencesite.create!(name: "Marketing und Management", user_id: U2.id, preference: "10")
P30=Preferencesite.create!(name: "Mikroökonomik", user_id: U2.id, preference: "10")
P31=Preferencesite.create!(name: "Öffentliche Finanzen", user_id: U2.id, preference: "10")
P32=Preferencesite.create!(name: "Personal und Arbeit", user_id: U2.id, preference: "10")
P33=Preferencesite.create!(name: "Produktionswirtschaft", user_id: U2.id, preference: "10")
P34=Preferencesite.create!(name: "Rechnungslegung und Wirtschaftsprüfung", user_id: U2.id, preference: "10")
P35=Preferencesite.create!(name: "Statistik", user_id: U2.id, preference: "10")
P36=Preferencesite.create!(name: "Umweltökonomik und Welthandel", user_id: U2.id, preference: "10")
P37=Preferencesite.create!(name: "Unternehmensführung und Organisation", user_id: U2.id, preference: "10")
P38=Preferencesite.create!(name: "Versicherungsbetriebslehre", user_id: U2.id, preference: "10")
P39=Preferencesite.create!(name: "Wirtschaftsinformatik", user_id: U2.id, preference: "10")
P40=Preferencesite.create!(name: "Wirtschaftspolitik", user_id: U2.id, preference: "10")

Translink.create!(preferencesite_id: P01.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P02.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P03.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P04.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P05.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P06.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P07.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P08.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P09.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P10.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P11.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P12.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P13.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P14.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P15.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P16.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P17.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P18.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P19.id, zuordnung: false, user_id: U1.id)
Translink.create!(preferencesite_id: P20.id, zuordnung: false, user_id: U1.id)

Translink.create!(preferencesite_id: P01.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P02.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P03.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P04.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P05.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P06.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P07.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P08.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P09.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P10.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P11.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P12.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P13.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P14.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P15.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P16.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P17.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P18.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P19.id, zuordnung: false, user_id: U2.id)
Translink.create!(preferencesite_id: P20.id, zuordnung: false, user_id: U2.id)
