$include options.inc
$include Output.inc

free variables
ZFW             Zielfunktionswert;

binary variables
x(j,i)          1 wenn die Arbeit des Studenten j von Institut i betreut wird;

equations
Zielfunktion
Kapazitaet
Zuordnung
;

Zielfunktion..
ZFW =e= sum((j,i), p(j,i) * x(j,i));

Kapazitaet(i)..
sum(j, x(j,i)) =l= (cp(i) + ch(i)) * (1+ci(i));

Zuordnung(j)..
sum(i, x(j,i)) =e= 1;

model BAZ /all/;
solve BAZ minimizing ZFW using mip;
$echo '' > Zuordnung1.txt
File Zuordnung1 /Zuordnung1.txt/;
put Zuordnung1;
loop(i, put loop(j, put j.tl, i.tl, x.l(j,i)/));
