json.extract! preferencesite, :id, :preference, :site_id, :created_at, :updated_at
json.url preferencesite_url(preferencesite, format: :json)
