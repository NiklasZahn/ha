# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
  $('#sites').dataTable({
    sPaginationType: "full_numbers"
    bJQueryUI: true
    "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/German.json" }
      });

jQuery ->
  $('.best_in_place').best_in_place()
