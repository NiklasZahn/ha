class Preferencesite < ApplicationRecord
  validates :preference, numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 10 }
  has_one :user
end
