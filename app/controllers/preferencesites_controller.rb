class PreferencesitesController < ApplicationController
  before_action :set_preferencesite, only: [:show, :edit, :update, :destroy]

  # GET /preferencesites
  # GET /preferencesites.json
  def index
    @preferencesites = Preferencesite.all
  end

  # GET /preferencesites/1
  # GET /preferencesites/1.json
  def show
  end

  # GET /preferencesites/new
  def new
    @preferencesite = Preferencesite.new
    @user = User.find_by(id: params[:id])
  end

  # GET /preferencesites/1/edit
  def edit
  end

  # POST /preferencesites
  # POST /preferencesites.json
  def create
    @preferencesite = Preferencesite.new(preferencesite_params)

    respond_to do |format|
      if @preferencesite.save
        format.html { redirect_to @preferencesite, notice: 'Preferencesite was successfully created.' }
        format.json { render :show, status: :created, location: @preferencesite }
      else
        format.html { render :new }
        format.json { render json: @preferencesite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /preferencesites/1
  # PATCH/PUT /preferencesites/1.json
  def update
    respond_to do |format|
      if @preferencesite.update(preferencesite_params)
        format.html { redirect_to @preferencesite, notice: 'Preferencesite was successfully updated.' }
        format.json { render :show, status: :ok, location: @preferencesite }
      else
        format.html { render :edit }
        format.json { render json: @preferencesite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /preferencesites/1
  # DELETE /preferencesites/1.json
  def destroy
    @preferencesite.destroy
    respond_to do |format|
      format.html { redirect_to preferencesites_url, notice: 'Preferencesite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_preferencesite
      @preferencesite = Preferencesite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def preferencesite_params
      params.require(:preferencesite).permit(:preference, :site_id, :user_id)
    end
end
