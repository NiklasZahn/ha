class StaticPagesController < ApplicationController
  def home
    @user = User.find_by(id: params[:id])
  end

  def help
  end

  def about
  end

  def contact
  end

  def institute
  end

  def optimize
    @user = User.find_by(id: params[:id])
  end
end
