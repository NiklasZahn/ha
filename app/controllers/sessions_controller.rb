class SessionsController < ApplicationController
  def new
  end


  def create
     user = User.find_by(matrikelnummer: params[:session][:matrikelnummer])
     admin = User.find_by(matrikelnummer: params[:session][:matrikelnummer], admin: true)
     if user && user.authenticate(params[:session][:password])
       if user.activated?
         log_in user
         session[:user_id] = user.id
         params[:session][:remember_me] == '1' ? remember(user) : forget(user)
         redirect_to home_path
       else
         message  = "Account nicht aktiviert."
         message += "Überprüfe deine Emails für den Aktivierungslink."
         flash[:warning] = message
         redirect_to root_path
       end
    elsif admin && user.authenticate(params[:session][:password])
       if user.activated?
         log_in user
         session[:user_id] = user.id
         params[:session][:remember_me] == '1' ? remember(user) : forget(user)
         redirect_to home_path
       else
         message  = "Account nicht aktiviert. "
         message += "Überprüfe deine Emails für den Aktivierungslink."
         flash[:warning] = message
         redirect_to root_path
       end
     else
       redirect_to root_url
       flash[:danger] = 'Falsche Matrikelnummer/Passwort Kombination'
     end
   end

   def destroy
     log_out if logged_in?
     redirect_to root_url
   end
end
