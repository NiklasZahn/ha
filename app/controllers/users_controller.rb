class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user=User.find(params[:id])
    @preferencesites = Preferencesite.all
    @preferencesitesByUser = @preferencesites.select{ |m| m.user_id == @user.id }
    @translinks = Translink.all
  end

  def new
    @user = User.new
  end

  def create
   @user = User.new(user_params)
   if @user.save
     Site.all.each {|si| Preferencesite.create!(name: si.name, user_id: @user.id, preference: "10")}
     @preferencesite= Preferencesite.find_by(user_id: @user.id)
     Site.all.each {|st| Translink.create!(preferencesite_id: @preferencesite.id, zuordnung: "0", user_id: @user.id)}
     @user.send_activation_email
     flash[:info] = "Bitte überprüfe deine Emails, um deinen Account zu aktivieren."
     redirect_to root_url
   else
     render 'new'
   end
 end

 def edit
 end

 def update
   if @user.update_attributes(user_params)
     flash[:success] = "Profil aktualisiert"
     redirect_to @user
   else
     render 'edit'
   end
 end

 def destroy
  @user=User.find(params[:id])
  Preferencesite.all.where(user_id: @user.id).destroy_all
  Translink.all.where(user_id: @user.id).destroy_all
  @user.destroy
  flash[:success] = "Benutzer entfernt"
  redirect_to users_url
  end

 private

   def user_params
     params.require(:user).permit(:name, :email, :password, :matrikelnummer,
                                  :password_confirmation)
   end

   def preferencesite_params
     params.require(:preferencesite).permit(:name, :preference, :user_id)
   end

   def profile
     redirect_to user_path(current_user)
   end

   # Before filters

   # Confirms the correct user.
   def correct_user
     @user = User.find(params[:id])
     redirect_to(root_url) unless current_user?(@user)
   end

   # Confirms an admin user.
   def admin_user
     redirect_to(home_url) unless current_user.admin?
   end

   def logged_in_user
     unless logged_in?
       flash[:danger] = 'Bitte einloggen.'
       redirect_to root_url
     end
   end

end
