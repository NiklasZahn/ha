class TranslinksController < ApplicationController
  before_action :set_translink, only: [:show, :edit, :update, :destroy]

  # GET /translinks
  # GET /translinks.json
  def index
    @translinks = Translink.all
  end

  # GET /translinks/1
  # GET /translinks/1.json
  def show
  end

  # GET /translinks/new
  def new
    @translink = Translink.new
  end

  # GET /translinks/1/edit
  def edit
  end

  # POST /translinks
  # POST /translinks.json
  def create
    @translink = Translink.new(translink_params)

    respond_to do |format|
      if @translink.save
        format.html { redirect_to @translink, notice: 'Translink was successfully created.' }
        format.json { render :show, status: :created, location: @translink }
      else
        format.html { render :new }
        format.json { render json: @translink.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /translinks/1
  # PATCH/PUT /translinks/1.json
  def update
    respond_to do |format|
      if @translink.update(translink_params)
        format.html { redirect_to @translink, notice: 'Translink was successfully updated.' }
        format.json { render :show, status: :ok, location: @translink }
      else
        format.html { render :edit }
        format.json { render json: @translink.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /translinks/1
  # DELETE /translinks/1.json
  def destroy
    @translink.destroy
    respond_to do |format|
      format.html { redirect_to translinks_url, notice: 'Translink was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def delete_zuordnung

    if File.exist?("Zuordnung1.txt")
      File.delete("Zuordnung1.txt")
    end
    @translinks = Translink.all
    @translinks.each { |li|
      li.zuordnung=0
      li.zuordnung=nil
      li.save
    }
    render :template => "translinks/index"
  end


  def read_zuordnung

    if File.exist?("Zuordnung1.txt")
      fi=File.open("Zuordnung1.txt", "r")
      fi.each { |line|
        if match = line.match(/j(\d+)[ ]+i(\d+)[ ]+(\d).00/)
          user, site, zbool = match.captures
          puts user
          puts site
          puts zbool
        else
          raise "Error" # nicht ganz richtig
        end
        al=Translink.find_by(preferencesite_id: site ,user_id: user)
        al.zuordnung = (zbool == "1")
        al.save
      }
      fi.close()
      @translinks = Translink.all
      render :template => "translinks/index"
    else
      flash.now[:not_available] = "Zuordnung wurde noch nicht berechnet!"
      @translinks = Translink.all
      render :template => "translinks/index"
    end
  end

  def optimize

    if File.exist?("Output.inc")
      File.delete("Output.inc")
    end
    f=File.new("Output.inc", "w")
    printf(f, "Sets" + "\n" + "i /\n")
    @sites = Site.order(:name)
    @sites.each { |s| printf(f, "i" + s.id.to_s + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "j /\n")
    @users = User.all
    @users.each { |usi| printf(f, "j" + usi.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")


    printf(f, "Parameters\ncp(i) /\n")

    @sites.each { |si| printf(f, "i" + si.id.to_s + "\t" + si.prof.to_s + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "\nch(i) /\n")

    @sites.each { |sit| printf(f, "i" + sit.id.to_s + "\t" + sit.hiwi.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "\nci(i) /\n")

    @sites.each { |sitc| printf(f, "i" + sitc.id.to_s + "\t" + sitc.capincr.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "Parameter\nTable\np(j,i)\n\t")
    @sites.each do |sitn|
      printf(f, "i" + sitn.id.to_s + "\t")
    end
    printf(f,"\n")

    @preferencesites = Preferencesite.order(:name)
    User.all.each do |u|
      uid = u.id
      printf(f, "j" + uid.to_s)
      @prefbyuser = @preferencesites.reorder(:name).select{ |m| m.user_id == uid }
      @prefbyuser.each do |sitn|
        rating = sitn.preference
        printf(f, "\t" + rating.to_s)
      end
      printf(f,"\n")
    end
    printf(f, ";" + "\n\n")
    f.close

    if File.exist?("Zuordnung1.txt")
      File.delete("Zuordnung1.txt")
    end

    system "gams Zuordnungsmodell.gms"

    @translinks = Translink.all
    flash.now[:started] = "Die Rechnung wurde gestartet!"
    render 'static_pages/home'

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_translink
      @translink = Translink.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def translink_params
      params.require(:translink).permit(:preferencesite_id, :zuordnung, :user_id)
    end
end
