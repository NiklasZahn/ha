require 'test_helper'

class TranslinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @translink = translinks(:one)
  end

  test "should get index" do
    get translinks_url
    assert_response :success
  end

  test "should show translink" do
    get translink_url(@translink)
    assert_response :success
  end

  test "should destroy translink" do
    assert_difference('Translink.count', -1) do
      delete translink_url(@translink)
    end

    assert_redirected_to translinks_url
  end
end
