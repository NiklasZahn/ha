require 'test_helper'

class PreferencesitesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @preferencesite = preferencesites(:one)
  end

  test "should get index" do
    get preferencesites_url
    assert_response :success
  end

  test "should get new" do
    get new_preferencesite_url
    assert_response :success
  end

  test "should get edit" do
    get edit_preferencesite_url(@preferencesite)
    assert_response :success
  end

  test "should destroy preferencesite" do
    assert_difference('Preferencesite.count', -1) do
      delete preferencesite_url(@preferencesite)
    end

    assert_redirected_to preferencesites_url
  end
end
