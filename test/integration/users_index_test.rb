require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:example)
  end


  test "index as non-admin" do
    log_in_as(@user)
    get users_path
  end
end
